//
//  ViewController.swift
//  Bitbox
//
//  Created by Светлов Андрей on 9/14/18.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
   
    @IBOutlet weak var rateDescriptionLabel: UILabel!
    
    @IBOutlet weak var rateLabel: UILabel!
    
    @IBOutlet weak var reverseRateDescriptionLabel: UILabel!
    
    @IBOutlet weak var currencyFromLabel: UILabel!
    
    @IBOutlet weak var currencyToLabel: UILabel!
    
    @IBOutlet weak var currencyListPicker: UIPickerView!
    
    let bitBox:BitboxApp = BitboxApp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateDescriptionLabel.text = ""
        rateLabel.text = "---"
        reverseRateDescriptionLabel.text = ""
        currencyFromLabel.text = "---"
        currencyToLabel.text = "---"
        
        currencyListPicker.delegate = self
        currencyListPicker.dataSource = self
    }
    
    @IBAction func refreshRateButtonPressed(_ sender: UIButton) {
        
    }
    
    
    @IBAction func getListCurrency(_ sender: UIButton) {
        //print(DataSource().getListCurrencies().count)
        currencyListPicker.isHidden = false
        
    }
    
}

extension UIViewController: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let result = DataSource().getListCurrencies().count
        
        return result
    }
}

extension UIViewController: UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       print(row)
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let currencies = DataSource().getListCurrencies()
        
        return currencies[row].code
    }
}





