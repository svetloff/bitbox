//
//  DataSource.swift
//  Bitbox
//
//  Created by Светлов Андрей on 9/17/18.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class DataSource: NSObject {

    func getListCurrencies() -> [Currency] {
        return getListCurrenciesLocal()
    }
    
    private func getListCurrenciesLocal() -> [Currency] {
        var result:[Currency] = []
        
        result.append(Currency(code: "USD", id: 840, name: "United States dollar", denominator: 100, numerator: 1, status: "active"))
        
        result.append(Currency(code: "EUR", id: 978, name: "EUR", denominator: 100, numerator: 1, status: "active"))
        
        result.append(Currency(code: "BTC", id: 2000, name: "BTC", denominator: 100000000, numerator: 1, status: "active"))
        
        result.append(Currency(code: "LTC", id: 2001, name: "LTC", denominator: 100000000, numerator: 1, status: "active"))
        
        return result
    }
    
    
    private func getListCurrenciesApi() -> [Currency] {
        let result:[Currency] = []
        
        return result
    }
    
    
    
}
