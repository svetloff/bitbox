//
//  BitboxApp.swift
//  Bitbox
//
//  Created by Светлов Андрей on 9/17/18.
//  Copyright © 2018 svetloff. All rights reserved.
//

struct Currency {
    let code: String
    let id: Int
    let name: String
    let denominator:  Int
    let numerator: Int
    let status: String
    
}

import UIKit

class BitboxApp: NSObject {
    var currencyFrom: Currency?
    var currencyTo: Currency?
    
    
}
